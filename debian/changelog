mediaelement (2.15.1+dfsg-3) unstable; urgency=medium

  * Upload to unstable

  [ Moritz Mühlenhoff ]
  * Use Python 3 (Closes: #937001, #967173)

  [ David Prévot ]
  * Use secure copyright file specification URI.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields:
    Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * Remove unnecessary get-orig-source-target.
  * Fix sections for source package mediaelement (web => javascript).
  * Set Rules-Requires-Root: no.
  * Become maintainer (ownCloud is not in Debian anmore)
  * Use debhelper-compat 13
  * Simplify dh_auto_build call
  * Update Standards-Version to 4.5.0
  * Use https in Homepage
  * Host repository on salsa
  * Rename main branch to debian/latest (DEP-14)
  * Update upstream Source URL

 -- David Prévot <taffit@debian.org>  Thu, 03 Sep 2020 15:52:18 -0400

mediaelement (2.15.1+dfsg-2) experimental; urgency=medium

  * Upload to experimental to respect the freeze
  * Move away from Debian JavaScript Maintainers team
  * Use repacksuffix feature of uscan
  * Add a get-orig-source target
  * Extend exclusion list to SWC
  * Drop Flash and Silverlight from available playback methods (Closes: #779491)
  * Bump standards version to 3.9.6

 -- David Prévot <taffit@debian.org>  Sun, 01 Mar 2015 12:20:31 -0400

mediaelement (2.15.1+dfsg-1) unstable; urgency=medium

  [ Luc Poupard ]
  * i18n : add French translation

  [ John Dyer ]
  * Moved updates to changelog.md
  * Added Flex build to Python build process to unify the building process
  * 2.15.1 build

  [ David Prévot ]
  * Add +dfsg suffix on repack
  * Split changelog from README

 -- David Prévot <taffit@debian.org>  Tue, 02 Sep 2014 21:26:50 -0400

mediaelement (2.14.2+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #733880)

 -- David Prévot <taffit@debian.org>  Wed, 01 Jan 2014 18:59:27 -0400
